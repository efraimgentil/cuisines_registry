package de.quandoo.recruitment.registry.exception;

public class CuisineNotFound extends CuisineRegistryException {

    public CuisineNotFound(String message) {
        super(message);
    }
}
