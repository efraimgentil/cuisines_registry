package de.quandoo.recruitment.registry.exception;

public class CuisineRegistryException extends RuntimeException {

    public CuisineRegistryException(String message) {
        super(message);
    }
}
