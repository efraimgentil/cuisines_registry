package de.quandoo.recruitment.registry;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.exception.CuisineNotFound;
import de.quandoo.recruitment.registry.exception.CuisineRegistryException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final Map<Cuisine, Set<Customer>> cuisineMap;

    public InMemoryCuisinesRegistry() {
        this.cuisineMap = new ConcurrentHashMap<>();
    }

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        notNull(customer);
        notNull(cuisine);
        cuisineMap.putIfAbsent(cuisine, Sets.newConcurrentHashSet());
        cuisineMap.computeIfPresent(cuisine, (cuisine1, customers) -> {
                customers.add(customer);
                return customers;
        });

    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        notNull(cuisine);
        if (!cuisineMap.containsKey(cuisine)) throw new CuisineNotFound("Cuisine does not exists");
        return ImmutableList.copyOf(cuisineMap.get(cuisine));
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        notNull(customer);
        synchronized (cuisineMap) {
            return cuisineMap.entrySet().stream()
                    .filter(e -> e.getValue().contains(customer))
                    .map(e -> e.getKey()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        //assuming that N is the number of cuisines to be returned
        return cuisineMap.entrySet().stream()
                .sorted((o1, o2) -> Integer.compare(o2.getValue().size(),o1.getValue().size()))
                .map(e -> e.getKey()).limit(n).collect(Collectors.toList());
    }

    private void notNull(Cuisine cuisine){
        if(cuisine == null) throw new CuisineRegistryException("Cuisine can not bue null");
    }

    private void notNull(Customer customer){
        if(customer == null) throw new CuisineRegistryException("Customer can not be null");
    }
}
