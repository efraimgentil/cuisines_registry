package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.exception.CuisineRegistryException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry;

    @Before
    public void setUp(){
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }

    @Test(expected = CuisineRegistryException.class)
    public void shouldThrowErrorIfCuisineIsNull() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test(expected = CuisineRegistryException.class)
    public void shouldThrowErrorIfCustomerIsNull() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void shouldReturnTheTopCuisines(){
        int n = 2;
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("italian"));

        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(n);

        assertThat(cuisines).hasSize(n);
        assertThat(cuisines.get(0)).isEqualTo(new Cuisine("italian"));
        assertThat(cuisines.get(1)).isEqualTo(new Cuisine("german"));
    }

    @Test
    public void shouldReturnCustomersFromTheCuisine(){
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));

        List<Customer> customers = cuisinesRegistry.cuisineCustomers(new Cuisine("german"));

        assertThat(customers).hasSize(3);
        assertThat(customers).containsOnly( new Customer("1") , new Customer("2") , new Customer("3"));
    }

    @Test
    public void shouldReturnCustomerCuisinesFromTheCuisine(){
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(new Customer("1"));

        assertThat(cuisines).hasSize(3);
        assertThat(cuisines).containsOnly( new Cuisine("german") , new Cuisine("italian") ,new Cuisine("french"));
    }

    @Test
    public void shouldReturnEmptyListIfThereIsNoCuisinesForTheCustomer(){
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(new Customer("4"));

        assertThat(cuisines).isEmpty();
    }

    @Test
    public void shouldRegisterNewCuisineIfItDoesNotExists(){
        cuisinesRegistry.register(new Customer("1"), new Cuisine("brazilian"));

        List<Customer> brazilian = cuisinesRegistry.cuisineCustomers(new Cuisine("brazilian"));

        assertThat(brazilian).hasSize(1);
        assertThat(brazilian).contains(new Customer("1"));
    }

    /**
     * Test intended to check concurrent modification and how far the inMemory storage can handle
     * there is no assertion as the intention is to stress the code to the max
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Ignore
    @Test
    public void checkForConcurrentModification() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        cuisinesRegistry.register(new Customer("1" ), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1" ), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1" ), new Cuisine("brazilian"));
        Future<?> registerThread = executorService.submit(() -> {
            List<Cuisine> cuisines = Arrays.asList(new Cuisine("french"), new Cuisine("german"), new Cuisine("brazilian"));
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                Collections.shuffle(cuisines);
                cuisinesRegistry.register(new Customer("1" + i), cuisines.get(0));
                System.out.println(i);
            }
        });
        Future<?> queryThread = executorService.submit(() -> {
            List<Cuisine> cuisines = Arrays.asList(new Cuisine("french"), new Cuisine("german"), new Cuisine("brazilian"));
            for (int i = 0; i < 1000; i++) {
                Collections.shuffle(cuisines);
                Cuisine cuisine = cuisines.get(0);
                System.out.println("#######-" + cuisine.getName());
                System.out.println(cuisinesRegistry.cuisineCustomers(cuisine).size());
            }
            System.out.println("End get");
        });
        registerThread.get();
        queryThread.get();
    }


}