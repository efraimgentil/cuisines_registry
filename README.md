# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

## Solution

1.  Refactor
    - Cleanup in the tests and giving meaningful names.
    - Adding HashCode and Equals to the models layer
    - Once registering a Customer in a Cuisine, if the cuisine does not exists, register a new one
    - Added AssertJ to the build.gradle
    - Added exceptions in the case of sent null values in the methods call
    - Returning and `ImmutableList` when querying for the Customers from a Cuisine, whit that the List can not be
    modified outside the `CuisinesRegistry` (de.quandoo.recruitment.registry.InMemoryCuisinesRegistry.cuisineCustomers)
2. Returning multiple cuisines
    - Looking in the CuisineMap to grab all cuisines from the users, as were used a HashSet implementation, the lookup is
      really fast O(1) and going through all cuisines takes O(n)
3. Top cuisines
    - I assumed that the N is the number of cuisines that should be returned in the list (it was not really clear).
    - I go through all cuisines and sort it from bigger to smaller then return only the number os cuisines requested
4. Change proposal
    - Make `cuisineCustomers` pageable
    - Move away from `InMemory` for a real Database solution (as the model mapping seems relational, an Postgres maybe).
    If `InMemory` is still an requirement an `Redis` cluster
    - Add more instances running CuisineRegistry and put a load-balancer in the front  